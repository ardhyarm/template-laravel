<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    //
    public function create() {
        return view('pertanyaan.create');
    }

    public function store(Request $request) {
        //dd($request->all());
        $request->validate([
            'title' => 'required|unique:pertanyaan',
            'isi' => 'required'
        ]);
        $query = DB::table('pertanyaan')->insert([
            "title" => $request["title"],
            "isi" => $request["isi"]
        ]);

        return redirect('/pertanyaan');
    }

    public function index() {
        $pertanyaan = DB::table('pertanyaan')->get();
        //dd($pertanyaan);
      
    }
}
