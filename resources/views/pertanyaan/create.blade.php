@extends('adminlte.master')
@section('title')
<h1>Tambah Pertanyaan</h1>
@endsection
@section('content')

              <div class="ml-3 mt-2">
              <form action="/pertanyaan" method="POST">
              @csrf
                
                  <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" name="judul" placeholder="masukkan title">
                 @error('judul')
                 <div class="alert alert-danger">{{ $messsage }}</div>
                 @enderror
                  </div>
                  <div class="form-group">
                        <label for="isi">Isi</label>
                        <textarea class="form-control" id="isi" name="isi" rows="5" ></textarea>
                        @error('isi')
                 <div class="alert alert-danger">{{ $messsage }}</div>
                 @enderror
                      
                      </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
              </form>
            </div>
            </div>
            
            @endsection
