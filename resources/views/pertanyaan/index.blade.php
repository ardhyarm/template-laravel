@extends('adminlte.master')
@section('title')
<h1>List Pertanyaan</h1>
@endsection
@section('content')
<div class="card-body">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Judul</th>
                      <th>Isi</th>
                      <th style="width: 40px">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                   @foreach($pertanyaan as $key=> $pertanyaan)
                        <tr>
                        <td>{{$key + 1}}</td>
                        <td> {{ $pertanyaan->Judul}}</td>
                        <td> {{ $pertanyaan ->$isi}}</td>
                        <td>{{}}</td>                        
                        </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
@endsection

